# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project: 	snkb
# Version: 	1.0.0
# Author:	  Joel Frehner
# Year:		  © 2018
#
# ============================================== #


# LANGNAV
lib.langNav = HMENU
lib.langNav {
	special = language
	special.value = 0,1,2
	special.normalWhenNoLanguage = 0
	wrap = <div class="language-navigation"><ul>|</ul></div>
	1 = TMENU
	1 {
		noBlur = 1
		NO = 1
		NO {
			linkWrap = <li>|</li> |*| <li>&#124;</li><li>|</li>
			stdWrap {
				override = De || Fr || En
				typolink {
					parameter.data = page:uid
					additionalParams = &L=0 || &L=1 || &L=2
					addQueryString = 1
					addQueryString.exclude = L,id,cHash,no_cache
					addQueryString.method = GET
					useCacheHash = 1
					no_cache = 0
				}
			}
			doNotLinkIt = 1
		}
		ACT < .NO
		ACT.linkWrap = <li class="lnactive">|</li> |*| <li>&#124;</li><li>|</li>
		USERDEF1 < .NO
		USERDEF1 {
			stdWrap.typolink >
			doNotShowLink = 1
		}
		USERDEF2 < .ACT
		USERDEF2 {
			stdWrap.typolink >
			doNotShowLink = 1
		}
	}
}
