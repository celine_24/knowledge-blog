# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project: 	snkb
# Version: 	1.0.0
# Author:	  Joel Frehner
# Year:		  © 2018
#
# ============================================== #

lib.navigation = HMENU
lib.navigation {
    wrap = <nav class="main-nav">|</nav>
    entryLevel = 0

    1 = TMENU
    1 {
        expAll = 1
        noBlur = 1
        wrap = <ul class="menu">|</ul>

        NO = 1
        NO {
            stdWrap.htmlSpecialChars = 1
            wrapItemAndSub = <li>|</li>
        }

        IFSUB < .NO
        IFSUB {
            wrapItemAndSub = <li class="has-sub-menu">|</li>
        }

        ACT < .NO
        ACT {
            wrapItemAndSub = <li class="active">|</li>
        }

        ACTIFSUB < .NO
        ACTIFSUB {
            wrapItemAndSub = <li class="has-sub-menu active">|</li>
        }
    }
    2 = TMENU
    2 {
        expAll = 1
        noBlur = 1

        wrap = <ul class="sub-menu">|</ul>

        NO {
            stdWrap.htmlSpecialChars = 1
            wrapItemAndSub = <li>|</li>
        }
        IFSUB < .NO
        IFSUB = 1
        IFSUB {
            wrapItemAndSub = <li class="menu-col">|</li>
        }
    }
    3 < .2
    3 {
        wrap = <ul>|</ul>
        IFSUB >
    }
}
