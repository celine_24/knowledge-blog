# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project: 	snkb
# Version: 	1.0.0
# Author:	  Joel Frehner
# Year:		  © 2018
#
# ============================================== #



# CUSTOMERINFORMATION
lib.companyName = TEXT
lib.companyName {
	value = {$plugin.snconfig.companyname}
}
lib.legalForm = TEXT
lib.legalForm {
	insertData = 1
	value = {LLL:EXT:snconfig/Resources/Private/Language/locallang.xlf:tx_snconfig.{$plugin.snconfig.legalform}}
}
lib.street = TEXT
lib.street {
	value = {$plugin.snconfig.street}
}
lib.streetNr = TEXT
lib.streetNr {
	value = {$plugin.snconfig.streetnr}
}
lib.country = TEXT
lib.country {
	value = {$plugin.snconfig.country}
}
lib.countryCode = TEXT
lib.countryCode {
	value = {$plugin.snconfig.countryCode}
}
lib.plz = TEXT
lib.plz {
	value = {$plugin.snconfig.plz}
}
lib.city = TEXT
lib.city {
	value = {$plugin.snconfig.city}
}
lib.phone = TEXT
lib.phone {
	value = {$plugin.snconfig.phone}
}
lib.mobile = TEXT
lib.mobile {
	value = {$plugin.snconfig.mobile}
}
lib.fax = TEXT
lib.fax {
	value = {$plugin.snconfig.fax}
}
lib.email = TEXT
lib.email {
	value = {$plugin.snconfig.email}
	typolink.parameter = {$plugin.snconfig.email}
	typolink.parameter.insertData = 1
	typolink.ATagParams = itemprop="email"
}
