# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project: 	snkb
# Version: 	1.0.0
# Author:	  Joel Frehner
# Year:		  © 2018
#
# ============================================== #



# LEGAL INFORMATIONS (Will be loaded in the fluidtemplate like this: © lib.year, lib.companyName, .webdesignBy, lib.impressum)
lib.year = TEXT
lib.year {
	data = date:U
	strftime = %Y
}
lib.webdesignBy = TEXT
lib.webdesignBy {
		value = Webdesign by sesamnet GmbH
		typolink.parameter = https://www.sesamnet.ch/webdesign/ _blank
}
lib.impressum = TEXT
lib.impressum {
	data = page:{$plugin.snconfig.impressum}
	typolink.parameter = {$plugin.snconfig.impressum}
}
