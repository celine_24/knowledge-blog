# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project: 	snkb
# Version: 	1.0.0
# Author:	  Joel Frehner
# Year:		  © 2018
#
# ============================================== #

lib.facebook = TEXT
lib.facebook {
		value = {$plugin.snconfig.facebook}
}

lib.instagram = TEXT
lib.instagram {
		value = {$plugin.snconfig.instagram}
}

lib.linkedin = TEXT
lib.linkedin {
		value = {$plugin.snconfig.linkedin}
}

lib.xing = TEXT
lib.xing {
		value = {$plugin.snconfig.xing}
}

lib.twitter = TEXT
lib.twitter {
		value = {$plugin.snconfig.twitter}
}

lib.youtube = TEXT
lib.youtube {
		value = {$plugin.snconfig.youtube}
}
