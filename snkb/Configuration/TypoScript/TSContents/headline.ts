# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project: 	snkb
# Version: 	1.0.0
# Author:	  Joel Frehner
# Year:		  © 2018
#
# ============================================== #


lib.headline = TEXT
lib.headline {
    value = {page:nav_title//page:title}
    insertData = 1
    wrap = <h1><mark>|</mark> - Übersicht</h1>
}