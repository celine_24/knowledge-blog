# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project: 	snkb
# Version: 	1.0.0
# Author:	  Joel Frehner
# Year:		  © 2018
#
# ============================================== #

tt_content.stdWrap.innerWrap.cObject >
tt_content.stdWrap.innerWrap.cObject = CASE
tt_content.stdWrap.innerWrap.cObject {
    key.field = layout
    1 = TEXT
    1 {
      value = <div class="layout-1">|</div>
    }
    2 = TEXT
    2 {
      value = <div class="layout-2">|</div>
    }
    3 = TEXT
    3 {
      value = <div class="layout-3">|</div>
    }
    100 = TEXT
    100 {
        value = <div class="accordion">|</div>
        stdWrap.prepend = COA
        stdWrap.prepend {
            stdWrap.wrap = <button class="accordion-trigger">|</button>
            10 = TEXT
            10 {
                value.field = header
            }
        }
    }
}
