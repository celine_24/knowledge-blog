# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project: 	snkb
# Version: 	1.0.0
# Author:	  Joel Frehner
# Year:		  © 2018
#
# ============================================== #


# ========================== #
# INCLUDE GLOBAL TYPOSCRIPTS #
# ========================== #
# TYPOSCRIPT OBJECTS (e.g. customerInformation, langNav, legalInfo etc.)
<INCLUDE_TYPOSCRIPT: source="DIR:EXT:snkb/Configuration/TypoScript/TSContents" extensions="ts">

# RENDERING CUSTOM TEMPLATE
page = PAGE
page {
    # Fluidtemplate
    typeNum = 0
    10 = FLUIDTEMPLATE
    10 {
        # Declarate location of Partial and Layouts folder
        partialRootPath = {$page.template.partialRootPath}
        layoutRootPath = {$page.template.layoutRootPath}

        # Selecting of the TYPO3 Backend Layouts
        file {
            stdWrap {
                cObject = CASE
                cObject {
                    key.data = levelfield:-1, backend_layout, slide
                    key.override.field = backend_layout

                    # Default Layout - Subpage with Subnav
                    default = TEXT
                    default {
                        value = {$page.template.templateRootPath}/Subpage.html
                    }

                    # Start/Homepage
                    pagets__homepageLayout = TEXT
                    pagets__homepageLayout {
                        value = {$page.template.templateRootPath}/Home.html
                    }

                    # Subpage without Sidebar
                    pagets__subpageLayout = TEXT
                    pagets__subpageLayout {
                        value = {$page.template.templateRootPath}/Subpage.html
                    }

                    # Subpage with Sidebar
                    pagets__sidebarLayout = TEXT
                    pagets__sidebarLayout {
                        value = {$page.template.templateRootPath}/SubpageSidebar.html
                    }

                }
            }
        }

        # Custom rendering of TYPO3 Contentelements (selected by row colpos)
        variables {

            # Render main content
            CONTENT < styles.content.get
            CONTENT {
                select.languageField = sys_language_uid
            }

            # Render sidebar content
            SIDEBAR = CONTENT
            SIDEBAR {
                table = tt_content
                select.where = colPos = 1
                slide = -1
                select.languageField = sys_language_uid
            }


        }
    }

    # Include Public files
    includeCSS {
        mainCss = {$cssDir}/main.css
        fontawesome = {$cssDir}/fontawesome.css
    }
    includeJSFooterlibs {
    }
    includeJSFooter {
        mainJs = {$jsDir}/main.js
    }
}



lib.contentElement {
   templateRootPaths {
      200 = EXT:snkb/Resources/Ext/fluid_styled_content/Templates/
   }
   partialRootPaths {
      200 = EXT:snkb/Resources/Ext/fluid_styled_content/Partials/
   }
   layoutRootPaths {
      200 = EXT:snkb/Resources/Ext/fluid_styled_content/Layouts/
   }
}



####################
# CUSTOM POWERMAIL #
####################
plugin.tx_powermail {
    view {
        templateRootPaths {
            0 = EXT:powermail/Resources/Private/Templates/
            1 = EXT:snkb/Resources/Ext/Powermail/Templates/
        }
        partialRootPaths {
            0 = EXT:powermail/Resources/Private/Partials/
            1 = EXT:snkb/Resources/Ext/Powermail/Partials/
        }
        layoutRootPaths {
            0 = EXT:powermail/Resources/Private/Layouts/
            1 = EXT:snkb/Resources/Ext/Powermail/Layouts/
        }
    }
}



###############
# CUSTOM NEWS #
###############
plugin.tx_news {
    view {
        templateRootPaths {
            100 = EXT:snkb/Resources/Ext/News/Templates/
        }

        partialRootPaths {
            100 = EXT:snkb/Resources/Ext/News/Partials/
        }

        layoutRootPaths {
            100 = EXT:snkb/Resources/Ext/News/Layouts/
        }
    }
}
