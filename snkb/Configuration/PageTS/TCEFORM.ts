# ============================================== #
#                                           _
#                                          | |
#  ___  ___  ___  __ _ _ __ ___  _ __   ___| |_
# / __|/ _ \/ __|/ _` | '_ ` _ \| '_ \ / _ \ __|
# \__ \  __/\__ \ (_| | | | | | | | | |  __/ |_
# |___/\___||___/\__,_|_| |_| |_|_| |_|\___|\__|
#
# Project: 	snkb
# Version: 	1.0.0
# Author:	  Joel Frehner
# Year:		  © 2018
#
# ============================================== #

TCEFORM {
    pages {
        layout {
            removeItems = 0, 1, 2, 3
            addItems {
                4 = Container
                5 = Container-Fluid
            }
            altLabels {
            }
        }
    }

    tt_content {
        # disable unnecessary options
        rowDescription.disabled = 1
        date.disabled = 1

        layout {
            addItems {
                100 = Akkordeon
            }
            altLabels {

            }
        }

        frame_class {
            removeItems = ruler-before, ruler-after, indent, indent-left, indent-right
            addItems {
                container = Container
                container-fluid = Container-Fluid
            }
        }
    }
}


######################
# CUSTOM NEWS LAYOUT #
######################
tx_news.templateLayouts {
    10 = Newsbox Teaser
    20 = News Slider
}
