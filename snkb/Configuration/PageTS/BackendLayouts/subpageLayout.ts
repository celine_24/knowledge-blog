mod.web_layout.BackendLayouts {
    subpageLayout {
        title = Subpage layout
        icon = EXT:snkb/Resources/Public/Icons/BackendLayouts/be_layout_1.png
        config {
            backend_layout {
                colCount = 1
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = content
                                colPos = 0
                            }
                        }
                    }
                }
            }
        }
    }
}
