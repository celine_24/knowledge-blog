mod.web_layout.BackendLayouts {
    sidebarLayout {
        title = Sidebar Layout
        icon = EXT:snkb/Resources/Public/Icons/BackendLayouts/be_layout_3.png
        config {
            backend_layout {
                colCount = 2
                rowCount = 1
                rows {
                    1 {
                        columns {
                            1 {
                                name = content
                                colPos = 0
                            }
                            2 {
                                name = sidebar
                                colPos = 1
                            }
                        }
                    }
                }
            }
        }
    }
}